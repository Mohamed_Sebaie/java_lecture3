package Info;

import java.util.Arrays;
import java.util.List;

public class Student extends User  {
	
	private String studentNumber = "";
	private String registretedCourse = "";
	private static String[] availableCourse = {"Java","C++", "Python"};
	
	public boolean checkCourseAvailability(String courseName) {
		
		List<String> list = Arrays.asList(availableCourse);
        
        if(list.contains(courseName)){
            return true;
        }
		return false;
		
	}
	
	public boolean registerCourse(String courseName) {
		
		if (checkCourseAvailability(courseName)) {
			
			this.registretedCourse = courseName;
			return true;
		}
		
		return false;
		
	}
	
	public void printRegistertedCourse() {
		
		System.out.println(this.registretedCourse);
	}

}
